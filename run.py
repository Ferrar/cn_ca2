import os
import pandas as pd
import matplotlib.pyplot as plt

agents = ["Agent/TCP/Newreno", "Agent/TCP", "Agent/TCP/Vegas"]
RUN_TIMES = 10
times = None
#cwnd1,cwnd2,rtt1,rtt2,goodput1,goodput2,packetLoss1,packetLoss2
parameters = [{a:[] for a in agents} for i in range(8)]

for agent in agents:
    for i in range(RUN_TIMES):
        os.system("ns main.tcl " + agent)
        file = pd.read_csv("varsFile.csv")
        times = file["time"]
        parameters[0][agent].append(file["cwnd1"])
        parameters[1][agent].append(file["cwnd2"])
        parameters[2][agent].append(file["rtt1"])
        parameters[3][agent].append(file["rtt2"])
        parameters[4][agent].append(file["goodput1"])
        parameters[5][agent].append(file["goodput2"])
        packetDrops = [[0 for j in range(len(times))] for t in range(2)]
        traces = pd.read_csv("traceFile.tr", sep=" ")
        traces.columns = ["event", "time", "d", "d", "d", "d", "d", "flow", "d", "d", "d", "d"]
        traces = traces[traces["event"] == "d"]
        for index, trace in traces.iterrows():
            packetDrops[trace["flow"]-1][int(trace["time"]*1.0/times[1])] += 1.0 / times[1]
        parameters[6][agent].append(packetDrops[0])
        parameters[7][agent].append(packetDrops[1])

for parameter in parameters:
    for agent in agents:
        parameter[agent] = [sum(j)/RUN_TIMES for j in zip(*parameter[agent])]

def plotSingle(paramNum, agent, title, yLabel, plotColor):
    global times, parameters
    _, ax = plt.subplots(1, 1, figsize=(16, 8))
    ax.plot(times, parameters[paramNum][agent], color=plotColor)
    ax.set_xlabel('Time', labelpad=9, size=14)
    ax.set_ylabel(yLabel, labelpad=9, size=14)
    ax.set_title(title, pad=9, weight='bold', size=14)
    plt.savefig("./plots/"+("_".join(title.split(" "))))

def groupPlot(Yvalues, title):
    global times
    _, ax = plt.subplots(1, 1, figsize=(18, 9))
    ax.plot(times, Yvalues[0], color="red", label="New-Reno Flow1")
    ax.plot(times, Yvalues[1], color="blue", label="New-Reno Flow2")
    ax.plot(times, Yvalues[2], color="orange", label="Tahoe Flow1")
    ax.plot(times, Yvalues[3], color="green", label="Tahoe Flow2")
    ax.plot(times, Yvalues[4], color="brown", label="Vegas Flow1")
    ax.plot(times, Yvalues[5], color="purple", label="Vegas Flow2")
    ax.set_xlabel('Time', labelpad=11, size=16)
    ax.set_ylabel("Parameters Values", labelpad=11, size=16)
    ax.set_title(title, pad=11, weight='bold', size=16)
    plt.legend()
    plt.savefig("./plots/"+("_".join(title.split(" "))))

plotSingle(0, "Agent/TCP/Newreno", "CWND New-Reno Flow1", "CWND", "red")
plotSingle(0, "Agent/TCP", "CWND Tahoe Flow1", "CWND", "blue")
plotSingle(0, "Agent/TCP/Vegas", "CWND Vegas Flow1", "CWND", "green")
plotSingle(1, "Agent/TCP/Newreno", "CWND New-Reno Flow2", "CWND", "red")
plotSingle(1, "Agent/TCP", "CWND Tahoe Flow2", "CWND", "blue")
plotSingle(1, "Agent/TCP/Vegas", "CWND Vegas Flow2", "CWND", "green")
plotSingle(2, "Agent/TCP/Newreno", "RTT New-Reno Flow1", "RTT", "red")
plotSingle(2, "Agent/TCP", "RTT Tahoe Flow1", "RTT", "blue")
plotSingle(2, "Agent/TCP/Vegas", "RTT Vegas Flow1", "RTT", "green")
plotSingle(3, "Agent/TCP/Newreno", "RTT New-Reno Flow2", "RTT", "red")
plotSingle(3, "Agent/TCP", "RTT Tahoe Flow2", "RTT", "blue")
plotSingle(3, "Agent/TCP/Vegas", "RTT Vegas Flow2", "RTT", "green")
plotSingle(4, "Agent/TCP/Newreno", "GoodPut New-Reno Flow1", "GoodPut", "red")
plotSingle(4, "Agent/TCP", "GoodPut Tahoe Flow1", "GoodPut", "blue")
plotSingle(4, "Agent/TCP/Vegas", "GoodPut Vegas Flow1", "GoodPut", "green")
plotSingle(5, "Agent/TCP/Newreno", "GoodPut New-Reno Flow2", "GoodPut", "red")
plotSingle(5, "Agent/TCP", "GoodPut Tahoe Flow2", "GoodPut", "blue")
plotSingle(5, "Agent/TCP/Vegas", "GoodPut Vegas Flow2", "GoodPut", "green")
plotSingle(6, "Agent/TCP/Newreno", "PacketLoss New-Reno Flow1", "PacketLoss", "red")
plotSingle(6, "Agent/TCP", "PacketLoss Tahoe Flow1", "PacketLoss", "blue")
plotSingle(6, "Agent/TCP/Vegas", "PacketLoss Vegas Flow1", "PacketLoss", "green")
plotSingle(7, "Agent/TCP/Newreno", "PacketLoss New-Reno Flow2", "PacketLoss", "red")
plotSingle(7, "Agent/TCP", "PacketLoss Tahoe Flow2", "PacketLoss", "blue")
plotSingle(7, "Agent/TCP/Vegas", "PacketLoss Vegas Flow2", "PacketLoss", "green")

groupPlot([parameters[0]["Agent/TCP/Newreno"], parameters[1]["Agent/TCP/Newreno"],
parameters[0]["Agent/TCP"], parameters[1]["Agent/TCP"],
parameters[0]["Agent/TCP/Vegas"], parameters[1]["Agent/TCP/Vegas"]], "CWND")

groupPlot([parameters[2]["Agent/TCP/Newreno"], parameters[3]["Agent/TCP/Newreno"],
parameters[2]["Agent/TCP"], parameters[3]["Agent/TCP"],
parameters[2]["Agent/TCP/Vegas"], parameters[3]["Agent/TCP/Vegas"]], "RTT")

groupPlot([parameters[4]["Agent/TCP/Newreno"], parameters[5]["Agent/TCP/Newreno"],
parameters[4]["Agent/TCP"], parameters[5]["Agent/TCP"],
parameters[4]["Agent/TCP/Vegas"], parameters[5]["Agent/TCP/Vegas"]], "GoodPut")

groupPlot([parameters[6]["Agent/TCP/Newreno"], parameters[7]["Agent/TCP/Newreno"],
parameters[6]["Agent/TCP"], parameters[7]["Agent/TCP"],
parameters[6]["Agent/TCP/Vegas"], parameters[7]["Agent/TCP/Vegas"]], "PacketLoss")

# plt.show()


