set ns [new Simulator]

proc finish {} {
    global ns namFile traceFile varsFile
    $ns flush-trace
    close $namFile
    close $traceFile
    close $varsFile
    exit 0
}

proc randelay {} {
    return [expr 5 + round(rand() * 20)]
}

set namFile [open namFile.nam w]
$ns namtrace-all $namFile

set traceFile [open traceFile.tr w]
$ns trace-all $traceFile

set n1 [$ns node]
set n2 [$ns node]
set n3 [$ns node]
set n4 [$ns node]
set n5 [$ns node]
set n6 [$ns node]

$ns duplex-link $n1 $n3 100Mb 5ms DropTail
$ns duplex-link $n2 $n3 100Mb [randelay]ms DropTail
$ns duplex-link $n3 $n4 100Kb 1ms DropTail
$ns duplex-link $n4 $n5 100Mb 5ms DropTail
$ns duplex-link $n4 $n6 100Mb [randelay]ms DropTail

$ns queue-limit $n3 $n4 10
$ns queue-limit $n4 $n5 10
$ns queue-limit $n4 $n6 10
$ns queue-limit $n4 $n3 10
$ns queue-limit $n3 $n1 10
$ns queue-limit $n3 $n2 10

$ns duplex-link-op $n1 $n3 orient right-down
$ns duplex-link-op $n2 $n3 orient right-up
$ns duplex-link-op $n3 $n4 orient right
$ns duplex-link-op $n4 $n5 orient right-up
$ns duplex-link-op $n4 $n6 orient right-down

$ns duplex-link-op $n3 $n4 queuePos 0.5
$ns duplex-link-op $n4 $n5 queuePos 0.5
$ns duplex-link-op $n4 $n6 queuePos 0.5

set tcp1 [new $argv]
$tcp1 set class_ 1
$ns attach-agent $n1 $tcp1
set end1 [new Agent/TCPSink]
$ns attach-agent $n5 $end1
$ns connect $tcp1 $end1
$tcp1 set ttl_ 64
$tcp1 set fid_ 1
$ns color 1 Blue

set tcp2 [new $argv]
$tcp2 set class_ 2
$ns attach-agent $n2 $tcp2
set end2 [new Agent/TCPSink]
$ns attach-agent $n6 $end2
$ns connect $tcp2 $end2
$tcp2 set ttl_ 64
$tcp2 set fid_ 2
$ns color 2 Red

set ftp1 [new Application/FTP]
$ftp1 attach-agent $tcp1
$ftp1 set type_ FTP

set ftp2 [new Application/FTP]
$ftp2 attach-agent $tcp2
$ftp2 set type_ FTP

$ns at 0 "$ftp1 start"
$ns at 0 "$ftp2 start"
$ns at 1000 "$ftp1 stop"
$ns at 1000 "$ftp2 stop"
$ns at 1000 "finish"

proc traceVariables {} {
    global ns tcp1 traceapp1 tcp2 traceapp2 varsFile

    set deltaTime 1
    set now [$ns now]

    set cwnd1 [$tcp1 set cwnd_]
    set cwnd2 [$tcp2 set cwnd_]

    set rtt1 [$tcp1 set rtt_]
    set rtt2 [$tcp2 set rtt_]

    set nbytes1 [$traceapp1 set bytes_]
    $traceapp1 set bytes_ 0
    set goodput1 [expr ($nbytes1 * 8.0 / 1000000) / $deltaTime]
    set nbytes2 [$traceapp2 set bytes_]
    $traceapp2 set bytes_ 0
    set goodput2 [expr ($nbytes2 * 8.0 / 1000000) / $deltaTime]

    puts $varsFile "$now,$cwnd1,$cwnd2,$rtt1,$rtt2,$goodput1,$goodput2"
    $ns at [expr $now+$deltaTime] "traceVariables"
}

Class TraceApp -superclass Application

TraceApp instproc init {args} {
    $self set bytes_ 0
    eval $self next $args
}

TraceApp instproc recv {byte} {
    $self instvar bytes_
    set bytes_ [expr $bytes_ + $byte]
    return $bytes_
}

set traceapp1 [new TraceApp]
set traceapp2 [new TraceApp]

$traceapp1 attach-agent $end1
$traceapp2 attach-agent $end2

$ns at 0 "$traceapp1 start"
$ns at 0 "$traceapp2 start"

set varsFile [open varsFile.csv w]
puts $varsFile "time,cwnd1,cwnd2,rtt1,rtt2,goodput1,goodput2"
$ns at 0 "traceVariables"

$ns run